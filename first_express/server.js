var express = require('express')
  , server = express()
  , UserController = require('./controller/UserController');

server.get('/', function(req, res) {
	res.send('Welcome, Participants! This is express, isn\'t?');
});

server.get('/user', function(req, res) {
	var controller = new UserController()
      , john = controller.findByName('John');
	
	res.send('User info: '+JSON.stringify(john));
});

module.exports = server;