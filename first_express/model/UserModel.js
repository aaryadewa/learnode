var dumpData = [{
		name: 'John',
		email: 'john.doe@gmail.com'
	}, {
		name: 'Jane',
		email: 'jane.doe@gmail.com'
	}];

exports.find = function(userObject) {
	console.info('Model is finding user data...');
	var dataCount = dumpData.length;
	
	for (var i = 0; i < dataCount; i++) {
		var user = dumpData[i];
		if (user.name === userObject.name) {
			return user;
		}
	}
	
	return null;
};

exports.getDumpData = function() {
	return dumpData;
};