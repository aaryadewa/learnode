var http = require('http')
  , UserController = require('./controller/UserController');

var server = http.createServer(function(req, res) {
	var method = req.method
	  , url = require('url').parse(req.url)
	  , pathname = url.pathname;
	
	res.setHeader('Content-Type', 'text/plain');
	res.statusCode = 200;
	
	// Root path will be our home.
	if (pathname === '/') {
		res.end('Welcome, Participants! This is our home, isn\'t?');
	}
	
	// Tell the application to find John for any request to /user pat.
	else if (pathname === '/user') {
		var controller = new UserController()
		  , john = controller.findByName('John');
	
		res.end(JSON.stringify(john));
	}
});

module.exports = server;