var http = require('http');

var server = http.createServer(function(req, res) {
	res.setHeader('Content-Type', 'text/plain');
	res.statusCode = 200;
	res.end('Welcome to NodeJS Training Session!');
});

module.exports = server;