var express = require('express')
  , server = express();

// Use the following routers.
var routesHome = require('./controller/index')
  , routesUser = require('./controller/user');

server.use('/', routesHome);
server.use('/user', routesUser);

module.exports = server;