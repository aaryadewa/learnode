var express = require('express')
  , router = express.Router();

var UserController = require('./UserController');

router.get('/', function(req, res) {
	var controller = new UserController()
    , john = controller.findByName('John');
	
	res.send('User info: '+JSON.stringify(john));
});

module.exports = router;