var http = require('http')
  , fs = require('fs')
  , ejs = require('ejs');

var server = http.createServer(function(req, res) {
	var options = {
		encoding: 'utf-8'
	};
	
	fs.readFile(__dirname+ '/template.ejs', options, function(err, content) {
		var str = ejs.render(content, {
			users: [{
				name: 'John Doe',
				email: 'john.doe@gmail.com'
			}, {
				name: 'Jane Doe',
				email: 'jane.doe@gmail.com'
			}]
		});

		res.setHeader('Content-Type', 'text/html');
		res.end(str);
	});
});

module.exports = server;