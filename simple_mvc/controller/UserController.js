var model = require('../model/UserModel');

function UserController(user) {
	if (typeof user !== 'undefined') {
		this.name = user.name;
		this.email = user.email;
	}
}

UserController.prototype.findByName = function(searchString) {
	console.log('Controller is finding ' +searchString+ '...');
	var record = model.find({name: searchString});
	return record;
};

module.exports = UserController;