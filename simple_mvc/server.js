var http = require('http');
var UserController = require('./controller/UserController');

var server = http.createServer(function(req, res) {
	var controller = new UserController();
	var john = controller.findByName('John');
	
	res.setHeader('Content-Type', 'text/plain');
	res.statusCode = 200;
	res.end(JSON.stringify(john));
});

module.exports = server;